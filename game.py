def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, board_no):
    if board[board_no(0)] == board[board_no(0)] and board[board_no(0+y)] == board[board_no(0+2*y)]:
        print_board(board, board_no)
        print(board[0], "has won")
        exit()

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"
y = [0, 1, 2, 3, 4]
board_no = board[0, 0+y, 0+2*y]



for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if board[0] == board[1] and board[1] == board[2]:
        game_over(board,board_no(0+y))

    elif board[3] == board[4] and board[4] == board[5]:
        print_board(board, board_no(3+1))
        print(board[3], "has won")

    elif board[6] == board[7] and board[7] == board[8]:
        print_board(board, board_no(6+1))
        print(board[6], "has won")

    elif board[0] == board[3] and board[3] == board[6]:
        print_board(board, board_no(0+3))
        print(board[0], "has won")

    elif board[1] == board[4] and board[4] == board[7]:
        print_board(board, board_no(0+3))
        print(board[1], "has won")

    elif board[2] == board[5] and board[5] == board[8]:
        print_board(board, board_no(0+3))
        print(board[2], "has won")

    elif board[0] == board[4] and board[4] == board[8]:
        print_board(board, board_no(0+4))
        print(board[0], "has won")

    elif board[2] == board[4] and board[4] == board[6]:
        print_board(board, board_no(0+2))
        print(board[0], "has won")

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
